﻿using Microsoft.AspNetCore.Mvc;
using SingletonMVC.Models;
using SingletonMVC.Services;
using System.Diagnostics;

namespace SingletonMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBookService _bookService;

        public HomeController(ILogger<HomeController> logger, IBookService bookService)
        {
            _logger = logger;
            _bookService = bookService;
        }

        public IActionResult Index()
        {
            var books = _bookService.GetBooks();

            return View(books);
        }

        public IActionResult CreateBook(Book book)
        {      
            return View("CreateBook");
        }

        [HttpPost]
        public IActionResult OnPostCreateBook(Book book)
        {
            if (!ModelState.IsValid || book == null)
            {
                return View("CreateBook", book);
            }

            _bookService.InsertBook(book);

            ViewBag.Message = "The book was created correctly";

            return View("CreateBook");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}