﻿using System.ComponentModel.DataAnnotations;

namespace SingletonMVC.Models
{
    public class Book
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Category { get; set; }
    }
}
