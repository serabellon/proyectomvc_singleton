﻿using SingletonMVC.Models;

namespace SingletonMVC.Repositories
{
    public interface IBooksRepository
    {
        IEnumerable<Book> GetBooks();
        void CreateBook(Book item);
    }
}
