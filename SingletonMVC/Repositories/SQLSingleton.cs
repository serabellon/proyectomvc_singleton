﻿using Microsoft.Data.SqlClient;

namespace SingletonMVC.Repositories
{
    public sealed class SQLSingleton
    {
        private static readonly SQLSingleton instance = new SQLSingleton();

        static SQLSingleton()
        {
        }

        private SQLSingleton()
        {
        }

        public static SQLSingleton Instance
        {
            get
            {
                return instance;
            }
        }

        public SqlConnection GetDBConnection(IConfiguration configuration)
        {
            return new SqlConnection(configuration.GetConnectionString("DefaultConnection"));
        }
    }
}
