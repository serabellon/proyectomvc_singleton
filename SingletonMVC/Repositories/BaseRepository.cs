﻿using System.Data;

namespace SingletonMVC.Repositories
{
    public abstract class BaseRepository
    {
        private readonly IConfiguration _configuration;

        protected BaseRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected IDbConnection getSQLConnection()
        {
            return SQLSingleton.Instance.GetDBConnection(_configuration);
        }
    }
}
