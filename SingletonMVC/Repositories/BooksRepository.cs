﻿using Dapper;
using SingletonMVC.Models;

namespace SingletonMVC.Repositories
{
    public class BooksRepository : BaseRepository, IBooksRepository
    {
        public BooksRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public void CreateBook(Book book)
        {
            var sql = "INSERT INTO Books ([Id], [Title], [Author], [Category]) VALUES " +
                "(@Id, @Title, @Author, @Category);";

            using var connection = getSQLConnection();
            connection.Execute(sql, book);
        }

        public IEnumerable<Book> GetBooks()
        {
            var sql = "SELECT * FROM Books;";

            using var connection = getSQLConnection();
            return connection.Query<Book>(sql);
        }
    }
}
