﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SingletonMVC.Models;

namespace SingletonMVC.Repositories
{
    public class SQLContext : DbContext
    {
        private readonly string _connectionString;
        public SQLContext(DbContextOptions<SQLContext> options, IConfiguration configuration) : base(options)
        {
            _connectionString = configuration.GetSection("ConnectionStrings:DefaultConnection").Value;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<Book> Books { get; set; }
    }
}
