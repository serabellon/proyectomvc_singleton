﻿using SingletonMVC.Models;
using SingletonMVC.Repositories;

namespace SingletonMVC.Services
{
    public class BookService : IBookService
    {
        private readonly IBooksRepository _booksRepository;

        public BookService(IBooksRepository booksRepository)
        {
            _booksRepository = booksRepository;
        }

        public IEnumerable<Book> GetBooks()
        {
            return _booksRepository.GetBooks();
        }

        public void InsertBook(Book book)
        {
            _booksRepository.CreateBook(book);
        }
    }
}
