﻿using SingletonMVC.Models;

namespace SingletonMVC.Services
{
    public interface IBookService
    {
        IEnumerable<Book> GetBooks();
        void InsertBook(Book book);
    }
}
